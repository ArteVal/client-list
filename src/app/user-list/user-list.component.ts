import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { User } from '../user';
import { UserFilterPipe } from '../user-filter.pipe';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private usersService: UsersService) { }
  ngOnInit(): void {
    this.getUsers();
  }
  
  search: string = '';
  users: User[];
  selectedUser: User;
  
  getUsers(): void {
    this.usersService.getUsers().then(users => this.users = users);
  }
  
  onSelect(user: User): void {
    this.usersService.selectuser(user);
    this.selectedUser = user;
  }
}
