import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { User } from '../user';

@Component({
  selector: 'user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  user : User;

  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    this.subscribeToSelection();
  }
  
  subscribeToSelection(): void {
    this.usersService.selectedUser$.subscribe(user => this.user = user);
  }
}
