import { User } from './user';
import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsersService {

  private selectedUserSource = new Subject<User>();
  selectedUser$ = this.selectedUserSource.asObservable();
  
  constructor (private http: Http){};
  
  getUsers(): Promise<User[]> {
    const url = 'assets/clients.json';
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as User[]);
  }
  
  selectuser(user: User) {
    this.selectedUserSource.next(user);
  }
  
}
