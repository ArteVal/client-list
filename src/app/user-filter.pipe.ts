import { Pipe, PipeTransform } from '@angular/core';
import { User } from './user';
@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

  transform(users: User[], search: string): any {
    if (users) return users.filter(
      user => {
        let ret = false;
        for (let key in user) {
          let item = user[key];
          for (let key in item)
          {
            if (item[key].toLowerCase().indexOf(search.toLowerCase()) !== -1) ret = true;
          }
        };
        return ret;
    });
  }

}
