import { TzMaxPage } from './app.po';

describe('tz-max App', () => {
  let page: TzMaxPage;

  beforeEach(() => {
    page = new TzMaxPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
